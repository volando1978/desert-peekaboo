﻿using UnityEngine;
using System.Collections;

public class movesInParallel : MonoBehaviour
{


	public int lifeTime;
	public float maxSpeed;

	Vector3 vectorRandom;

	int ovniType;

	// Use this for initialization
	void Start ()
	{
		ovniType = Random.Range (0, 10);
		print ("OVNI TYPE: " + ovniType);
		vectorRandom = new Vector3 (floatRandom (), floatRandom (), floatRandom ());
		maxSpeed = Random.Range (0.1f, maxSpeed);

	}
	
	// Update is called once per frame
	void Update ()
	{
		GameObject g = GameObject.FindGameObjectWithTag ("center");

		if (ovniType > 5) {
			transform.position = new Vector3 (transform.position.x, 0.5f, transform.position.z);
		}
	
		transform.Translate (vectorRandom * Time.deltaTime * maxSpeed);
		lifeTime -= 1;

		if (lifeTime < 0) {
//			if (ovniType < 5) {

			Destroy (gameObject);
//			}
		}
	}

	float floatRandom ()
	{
		return (Random.Range (1, 360));
	}
}
