﻿using UnityEngine;
using System.Collections;

public class grassManager : MonoBehaviour
{



	public GameObject herb, cilinder;
	public int maxHerb, minScale, maxScale;
	public float separation;
	float randomFactor;

	public ArrayList grass;
	public ArrayList meshArray;


	public Mesh herbMesh;
	public Mesh stone;
	public Mesh cupula;
	public Mesh chimey;





	// Use this for initialization
	void Start ()
	{
		grass = new ArrayList ();
		meshArray = new ArrayList ();
		fillMeshArray ();

		placeBigCilinder ();

		for (int i = 0; i< maxHerb; i++) {
			randomFactor = Random.Range (1, 10);
			Vector3 pos = new Vector3 ((i % 6) * separation * randomFactor, 0, (i / 6) * separation * randomFactor);

			GameObject herbUnit = Instantiate (herb, pos, Quaternion.identity) as GameObject;
//			herbUnit.AddComponent<Renderer> ();
			grass.Add (herbUnit);
			assignMesh ((GameObject)grass [i]);
			assingRot ((GameObject)grass [i]);
			assignScale ((GameObject)grass [i]);

		}
	}
	
	void placeBigCilinder ()
	{
		Vector3 vectorRandom = new Vector3 (floatRandom (), 300, floatRandom ());
		var rot = Quaternion.Euler (90, 0, 0);
		GameObject ovniRepe = Instantiate (cilinder, vectorRandom, Quaternion.identity) as GameObject;
		ovniRepe.transform.rotation = rot;

	}
	
	float floatRandom ()
	{
		return (Random.Range (1, 360));
	}



	void fillMeshArray ()
	{
		meshArray.Add (herbMesh);
		meshArray.Add (stone);
		meshArray.Add (cupula);
		meshArray.Add (chimey);
		
	}

	public void assignTras (GameObject g)
	{
		Vector3 vNew = new Vector3 (0, 0, 0);
		g.transform.position = vNew;
	}
	
	public void assingRot (GameObject g)
	{
		Vector3 rotPos = new Vector3 (0, Random.Range (0, 360), 0);
		g.transform.Rotate (rotPos);
	}
	
	public void assignColor (GameObject g)
	{
//		MeshFilter r = g.GetComponent<MeshFilter> ();
		
		
		//				stencilColor = new Color (0f, 0f, 0f, 1);//mod to get 0.5 to 1
		
		//				if (currentState == state.f1) {
		
		//				float randomRangeColor = Random.Range (0f, 1f);
		//				c = new Color (randomRangeColor, randomRangeColor, randomRangeColor, 1);//mod to get 0.5 to 1
		
		//				}
		
		//				if (currentState == state.feedback) {
		float randomRangeColorR = Random.Range (0f, 255f);
		float randomRangeColorG = Random.Range (0f, 255f);
		float randomRangeColorB = Random.Range (0f, 255f);
		Color stencilColor = new Color (randomRangeColorR, randomRangeColorG, randomRangeColorB, 1);//mod to get 0.5 to 1
		
		
		//				}
//		r.color = stencilColor;
//		g.GetComponent<Renderer> ().color = stencilColor;
		
	}

	public void assignRot (GameObject g)
	{
		Quaternion vNew = Quaternion.Euler (transform.rotation.x, Random.Range (0, 359), transform.rotation.z);//getRandomFloat (distanciaOrigen2 * 2, distanciaOrigen2)
		
		
		g.transform.rotation = vNew;
	}
	
	public void assignScale (GameObject g)
	{
		float sc = Random.Range (minScale, maxScale);
		
		Vector3 vNew = new Vector3 (sc, sc, sc);
		
		
		g.transform.localScale = vNew;
	}
	
	
	public void assignMesh (GameObject g)
	{
		
		g.GetComponent<MeshFilter> ().sharedMesh = meshArray [Random.Range (0, meshArray.Count)] as Mesh;// g.GetComponent<MeshCollider> ().sharedMesh =
		
	}




}
